package com.requests.portal.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
public class TicketResponse implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column( length = 1000 )
    private String comments;

    private Date responseTime;

    @ManyToOne
    @JoinColumn( name = "request_id", nullable = false )
    private Request request;

    @ManyToOne
    @JoinColumn( name="client_id", nullable = false)
    private Client client;

}
