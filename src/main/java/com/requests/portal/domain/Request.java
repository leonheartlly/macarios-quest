package com.requests.portal.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
public class Request implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column( length = 50, nullable = false )
    private String subject;

    private int agency;

    @Column( length = 500, nullable = false )
    private String description;

    private Date requestTime;

    private Date endingTime;

    private int status;

    private int severity;

    @OneToMany( mappedBy = "request" )
    private Set<TicketResponse> responses = new HashSet<>();

    @ManyToOne
    @JoinColumn(  name = "client_id", nullable = false )
    private Client client;


}
