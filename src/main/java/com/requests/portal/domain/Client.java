package com.requests.portal.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.requests.portal.domain.enuns.Agency;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
public class Client implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column( length = 50, nullable = false )
    private String name;

    @Column(length = 35, unique = true, nullable = false)
    private String username;

    @Column( length = 500, nullable = false )
    private String pwd;

    @Column( length = 35)
    private String email;

    @Column( length = 11)
    private String fone;

    @ToString.Exclude
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="client_role",
            joinColumns = @JoinColumn(name = "client_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private List<Role> roles = new ArrayList<>();

    private int agency;

    public boolean enabled;

    public int retry;

    @JsonIgnore
    @OneToMany( mappedBy = "client" )
    private Set< Request > requests = new HashSet<>();

    @OneToMany(mappedBy = "client")
    private Set<TicketResponse> requestResponse = new HashSet<>();

    public Client() {
    }

    public Client(Client client){
        super();
        this.username = client.getUsername();
        this.name = client.getName();
        this.pwd = client.getPwd();
        this.roles = client.getRoles();
        this.fone = client.getFone();
        this.email = client.getEmail();
        this.agency = client.getAgency().getCode();
        this.retry = client.getRetry();
        this.roles = client.getRoles();
        this.enabled = client.isEnabled();
    }

    public Agency getAgency() {
        return Agency.getAgency(this.agency);
    }

    public void setAgency(Agency agency) {
        this.agency = agency.getCode();
    }
}
