package com.requests.portal.domain.enuns;


import java.util.Optional;


public enum Severity {

    SIMPLE( 1, "SIMPLES" ),
    MIDDLE( 2, "MEDIA" ),
    HIGH(3, "ALTA"),
    DISASTER( 4, "FUDEU" );

    private int code;

    private String desc;


    Severity(int code, String desc ) {

        this.code = code;
        this.desc = desc;
    }


    public int getCode() {

        return code;
    }


    public String getDesc() {

        return desc;
    }


    public static Severity getPedidoStatus(Integer cod ) {

        if ( !Optional.ofNullable( cod ).isPresent() ) {
            return null;
        }

        for ( Severity ps : Severity.values() ) {
            if ( cod.equals( ps.getCode() ) ) {
                return ps;
            }
        }
        throw new IllegalArgumentException( "Id inválido: " + cod );
    }

}
