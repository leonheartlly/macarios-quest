package com.requests.portal.domain.enuns;


import java.util.Optional;


public enum Status {

    OPEN(1,"ABERTO"),
    IN_PROGRESS( 2, "EM_ATENDIMENTO" ),
    CANCELED( 3, "CANCELADO" ),
    FINISHED(4, "ENCERRADO"),
    WAITING_PARTS( 5, "AGUARDANDO PEÇA" ),
    WAITING_FOR_MANAGER( 6, "AGUARDANDO PERMISSÃO DO GESTOR" );

    private int code;

    private String desc;


    Status(int code, String desc ) {

        this.code = code;
        this.desc = desc;
    }


    public int getCode() {

        return code;
    }


    public String getDesc() {

        return desc;
    }


    public static Status getPedidoStatus(Integer cod ) {

        if ( !Optional.ofNullable( cod ).isPresent() ) {
            return null;
        }

        for ( Status ps : Status.values() ) {
            if ( cod.equals( ps.getCode() ) ) {
                return ps;
            }
        }
        throw new IllegalArgumentException( "Id inválido: " + cod );
    }

}
