package com.requests.portal.domain.enuns;


import java.util.Optional;


public enum Agency {

    ORGAO(0, "Nenhum"),
    FMS_PEDRO_TAVARES(1, "UBS - Pedro Tavares Sobrinho"),
    FMS_JOSE_CANDIDO(2, "UBS - Jose Candido Gomes"),
    FMS_MARIA_CANDIDA(3, "UBS - Maria Candida"),
    FMS_SECRETARIA_SAUDE(4, "FMS - Secretaria Saude"),
    FUNASA(5, "FUNASA"),
    FARMACIA(6, "Farmacia"),
    PREFEITURA(7, "ADM - Prefeitura"),
    CONSELHO_TUTELAR(8, "ADM - Conselho Tutelar"),
    GARAGEM(9, "ADM - Garagem"),
    CRAS(10, "FMA - CRAS"),
    ASSISTENCIA(11, "FMA - Assistencia"),
    ESCOLA_ALDENORA_M(12, "EDU - Escola Aldenora M."),
    ESCOLA_BRICIO_L(13, "EDU - Escola Bricio L."),
    ESCOLA_BAIANOPOLIS(14, "EDU - Escola Baianópolis"),
    ESCOLA_CIMEI(15, "EDU - Escola Cimei"),
    SECRETARIA_DA_EDUCACAO(16, "EDU - Secretaria da Educação");

    private int code;

    private String desc;


    Agency(int code, String desc) {

        this.code = code;
        this.desc = desc;
    }


    public int getCode() {

        return code;
    }


    public String getDesc() {

        return desc;
    }


    public static Agency getAgency(Integer cod) {

        if (!Optional.ofNullable(cod).isPresent()) {
            return null;
        }

        for (Agency ps : Agency.values()) {
            if (cod.equals(ps.getCode())) {
                return ps;
            }
        }
        throw new IllegalArgumentException("Id inválido: " + cod);
    }

}
