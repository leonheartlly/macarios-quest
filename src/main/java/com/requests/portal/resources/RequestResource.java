package com.requests.portal.resources;


import com.requests.portal.domain.Request;
import com.requests.portal.dto.NewRequestDTO;
import com.requests.portal.dto.RequestDTO;
import com.requests.portal.service.RequestService;
import io.swagger.annotations.ApiOperation;
import javassist.tools.rmi.ObjectNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@RestController
@RequestMapping(value = "/chamados")
public class RequestResource {


    @Autowired
    private RequestService requestService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<RequestDTO>> listAll() {

        List<Request> clients = requestService.listAll();
        List<RequestDTO> clientDTOS = clients.stream().map(obj -> new RequestDTO(obj)).collect(Collectors.toList());

        return ResponseEntity.ok().body(clientDTOS);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<RequestDTO> findById(@PathVariable Long id) {

        Request request = requestService.findById(id);
        RequestDTO requestDTO = new RequestDTO(request);

        return ResponseEntity.ok().body(requestDTO);
    }

    @ApiOperation("Adiciona um ticket")
    @Secured({"ROLE_ADMIN"})
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> add(@Valid @RequestBody NewRequestDTO requestDTO) {

        requestDTO.setId(null);
        Request request = requestService.convertDTO(requestDTO);

        request = requestService.insert(request);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(request.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@Valid @RequestBody RequestDTO requestDTO, @PathVariable Long id) {

        Request request = requestService.convertDTO(requestDTO);
        request.setId(id);

        request = requestService.update(request);

        return ResponseEntity.noContent().build();
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {

        requestService.delete(id);
        return ResponseEntity.noContent().build();
    }


}
