package com.requests.portal.resources;


import com.requests.portal.domain.Client;
import com.requests.portal.dto.user.ClientDTO;
import com.requests.portal.dto.user.NewClientDTO;
import com.requests.portal.dto.user.UserPasswordChange;
import com.requests.portal.service.ClientService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@RestController
@RequestMapping(value = "/usuarios")
public class ClientResource {


    @Autowired
    private ClientService clientService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ModelMapper mapper;


    @ApiOperation("Lista todos os usuários")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<ClientDTO>> listAll() {

        List<Client> clients = clientService.listAll();
        List<ClientDTO> clientDTOS = clients.stream().map(obj -> new ClientDTO(obj)).collect(Collectors.toList());

        return ResponseEntity.ok().body(clientDTOS);
    }

    @ApiOperation("Pesquisa um usuário")
    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<ClientDTO> findById(@PathVariable Long id) {

        Client client = clientService.findById(id);
        ClientDTO clientDTO = new ClientDTO(client);

        return ResponseEntity.ok().body(clientDTO);
    }

    @ApiOperation("Adiciona novos usuários")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Usuário adicionado com sucesso")})
    @Secured({})
    @RequestMapping(value = "/novo-usuario", method = RequestMethod.POST)
    public ResponseEntity<Void> add(@Valid @RequestBody NewClientDTO clientDTO) {

        clientDTO.setId(null);
        Client client = clientService.convertDTO(clientDTO);

        client = clientService.insert(client);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(client.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@Valid @RequestBody NewClientDTO clientDTO, @PathVariable Long id) {

        Client client = clientService.convertDTO(clientDTO);
        client.setId(id);

        client = clientService.update(client);

        return ResponseEntity.noContent().build();
    }

    @ApiOperation("Altera um usuário específico")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<Void> customUpdate(@NotNull @RequestBody ClientDTO clientDTO, @NotNull @PathVariable Long id) {

        Client client = new Client();

        client.setId(id);
        mapper.map(clientDTO, client);
        client.setRoles(clientService.findRoles(clientDTO.getRoles()));

        client = clientService.update(client);

        return ResponseEntity.noContent().build();
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @PutMapping(value = "change-password/")
    public ResponseEntity<Void> updatePassword(@Valid @RequestBody UserPasswordChange userPasswordChange) {

        Client client = clientService.passwordChange(userPasswordChange);

        return ResponseEntity.noContent().build();
    }

    @ApiOperation("Deleta um usuário")
    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {

        clientService.delete(id);
        return ResponseEntity.noContent().build();
    }


}
