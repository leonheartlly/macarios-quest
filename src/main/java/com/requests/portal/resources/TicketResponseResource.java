package com.requests.portal.resources;


import com.requests.portal.domain.TicketResponse;
import com.requests.portal.dto.TicketResponseDTO;
import com.requests.portal.service.TicketResponseService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@Log4j2
@RestController
@RequestMapping(value = "/ticket-respostas")
public class TicketResponseResource {


    @Autowired
    private TicketResponseService ticketResponseService;

    @ApiOperation("Lista todos os comentários de um ticket")
    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/{clientId}/{requestId}", method = RequestMethod.GET)
    public ResponseEntity<List<TicketResponse>> listAll(@PathVariable Long clientId, @PathVariable Long requestId) {

        List<TicketResponse> clients = ticketResponseService.listAllByRequestIdAndClientId(requestId, clientId);
//        List<RequestDTO> clientDTOS = clients.stream().map(obj -> new RequestDTO(obj)).collect(Collectors.toList());

        return ResponseEntity.ok().body(clients);
    }

    @ApiOperation("Busca um comentário")
    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<TicketResponse>> findById(@PathVariable Long id) {

        List<TicketResponse> request = ticketResponseService.listAllByRequestId(id);
//        RequestDTO requestDTO = new RequestDTO(request);

        return ResponseEntity.ok().body(request);
    }

    @ApiOperation("Adiciona um comentário a um ticket")
    @Secured({"ROLE_ADMIN"})
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> add(@Valid @RequestBody TicketResponseDTO requestDTO) {

        requestDTO.setId(null);
//        Request request = requestService.convertDTO(requestDTO);

        TicketResponse comment = ticketResponseService.insert(requestDTO);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(comment.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

}
