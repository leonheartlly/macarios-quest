package com.requests.portal.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.requests.portal.domain.Client;
import com.requests.portal.domain.Role;
import com.requests.portal.domain.enuns.Agency;
import com.requests.portal.service.validator.ContactInsert;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@ContactInsert
public class NewClientDTO implements Serializable, GenericClient  {

    @JsonIgnore
    private Long id;

    @Length(min=5, max=30, message = "A name deve ter entre 5 e 30 caracteres.")
    @NotBlank(message = "O Nome não pode ficar em branco.")
    @Pattern(regexp="[a-zA-Z\\s]*", message = "O Nome deve conter apenas letras.")
    private String name;

    @Length(min=6, max=10, message = "A senha deve ter entre 6 e 10 dígitos.")
    @NotBlank(message = "A Senha não pode ficar em branco.")
    @Pattern(regexp="[0-9]*", message = "A senha deve conter apenas números.")
    private String password;

    @Email(message = "Email informado não reconhecido.")
    @NotBlank(message = "O Email não pode ficar em branco.")
    @Length(min = 10, max = 25, message = "O email deve ter pelo menos 5 caracteres.")
    private String email;

    @NotBlank(message = "O Usuário não pode ficar em branco.")
    @Length(min = 5, message = "O usuário deve ter pelo menos 5 caracteres.")
    @Pattern(regexp="[a-zA-Z]*", message = "O Usuário deve conter apenas letras.")
    private String username;

    @Length(min = 8, message = "O fone deve ter pelo menos 8 dígitos.")
    @NotBlank(message = "O Telefone não pode ficar em branco.")
    @Pattern(regexp="[0-9]*", message = "O Telefone deve conter apenas números.")
    private String fone;

    @NotNull(message = "Orgão não pode ser vazio.")
    private Agency agency;

    private List<Long> roles = new ArrayList<>();


    public NewClientDTO() {
    }

    public NewClientDTO(Client client) {
        this.id = client.getId();
        this.name = client.getName();
        this.password = client.getPwd();
        this.email = client.getEmail();
        this.fone = client.getFone();
        this.username = client.getUsername();
//        this.roles = client.getRoles();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
                //passwordEncoder.encode(password);
    }


    @Override
    public String toString() {
        return " roles=" + roles;
    }
}
