package com.requests.portal.dto.user;

import com.requests.portal.domain.Client;
import org.springframework.security.core.userdetails.UserDetails;

public interface PortalUserDetails extends UserDetails {

    Long getId();

    boolean getEnabled();

    Client getClient();
}
