package com.requests.portal.dto.user;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
public class UserPasswordChange implements Serializable {

    @Email(message = "Email informado não reconhecido.")
    @NotBlank(message = "O Email não pode ficar em branco.")
    @Length(min = 10, max = 25, message = "O email deve ter pelo menos 5 caracteres.")
    private String email;

    @NotBlank(message = "O Usuário não pode ficar em branco.")
    @Length(min = 5, message = "O usuário deve ter pelo menos 5 caracteres.")
    @Pattern(regexp="[a-zA-Z]*", message = "O Usuário deve conter apenas letras.")
    private String username;

    @Length(min=6, max=10, message = "A senha deve ter entre 6 e 10 dígitos.")
    @NotBlank(message = "A Senha não pode ficar em branco.")
    @Pattern(regexp="[0-9]*", message = "A senha deve conter apenas números.")
    private String password;

}
