package com.requests.portal.dto.user;

import com.requests.portal.domain.enuns.Agency;

public interface GenericClient {

    Agency getAgency();
}
