package com.requests.portal.dto.projections;

public interface ClientPasswordChange {

    String ACCEPT_HEADER = "application/client-password+json";

    String getEmail();
    String getPassword();
    String getUsername();
}
