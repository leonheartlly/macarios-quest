package com.requests.portal.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.requests.portal.domain.Request;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestDTO {

    private Long id;

    @JsonProperty("assunto")
    private String subject;


    private int agency;

    @JsonProperty("descricao")
    private String description;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm")
    private Date requestTime;

    private int status;


    public RequestDTO() {
    }

    public RequestDTO(Request request) {
        this.id = request.getId();
        this.subject = request.getSubject();
        this.agency = request.getAgency();
        this.description = request.getDescription();
        this.requestTime = request.getRequestTime();
        this.status = request.getStatus();
    }
}
