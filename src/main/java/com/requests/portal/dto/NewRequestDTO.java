package com.requests.portal.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.requests.portal.domain.Request;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NewRequestDTO {

    private Long id;

    @Length(min = 5, max = 30, message = "A assunto deverá ter entre 4 e 30 caracteres.")
    @NotEmpty(message = "O assunto não pode ser vazia.")
    private String subject;


    @NotNull
    private int severity;

    @NotEmpty(message = "A descrição não pode ser vazia.")
    @Length(min = 10, max = 500, message = "A descrição deverá ter entre 10 e 500 caracteres.")
    private String description;

    public NewRequestDTO() {
    }

    public NewRequestDTO(Request request) {
        this.id = request.getId();
        this.subject = request.getSubject();
        this.severity = request.getSeverity();
        this.description = request.getDescription();
    }
}
