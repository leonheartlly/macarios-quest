package com.requests.portal.dto;

import com.requests.portal.domain.Client;
import com.requests.portal.domain.Request;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class TicketResponseDTO implements Serializable {

    private Long id;

    private String comments;

    private Date responseTime;

    private Long requestId;

    private Long clientId;

}
