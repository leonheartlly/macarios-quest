package com.requests.portal;

import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class PortalApplication {


	@Bean
	public ModelMapper modelMapper() {
		ModelMapper map  = new ModelMapper();
		map.getConfiguration().setPropertyCondition(Conditions.isNotNull());
		map.getConfiguration()
				.setMatchingStrategy(MatchingStrategies.STRICT);


		return map;
	}


//	@Bean
//	public WebMvcConfigurer corsConfigurer() {
//		return new WebMvcConfigurer() {
//			@Override
//			public void addCorsMappings(CorsRegistry registry) {
//				registry.addMapping("/**").allowedOrigins("http://localhost:3000");
//			}
//		};
//	}

	public static void main(String[] args) {
		SpringApplication.run(PortalApplication.class, args);
	}

}
