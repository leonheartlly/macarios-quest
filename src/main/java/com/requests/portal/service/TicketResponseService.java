package com.requests.portal.service;

import com.requests.portal.domain.Client;
import com.requests.portal.domain.Request;
import com.requests.portal.domain.TicketResponse;
import com.requests.portal.domain.enuns.Status;
import com.requests.portal.dto.NewRequestDTO;
import com.requests.portal.dto.RequestDTO;
import com.requests.portal.dto.TicketResponseDTO;
import com.requests.portal.dto.user.PortalUserDetails;
import com.requests.portal.repository.ClientRepository;
import com.requests.portal.repository.RequestRepository;
import com.requests.portal.repository.TicketResponseRepository;
import com.requests.portal.service.Exceptions.DataIntegrityException;
import com.requests.portal.service.Exceptions.ObjectNotFoundException;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class TicketResponseService {


    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private TicketResponseRepository ticketResponseRepository;


    @Autowired
    private ClientRepository clientRepository;


    public List<TicketResponse> listAllByRequestId(Long requestId) {

        List<TicketResponse> ticketResponses = ticketResponseRepository.findByRequestId(requestId);
        log.info(ticketResponses.size() + " respostas encontradas para o ticket " + requestId);

        return ticketResponses;
    }

    public List<TicketResponse> listAllByRequestIdAndClientId(Long requestId, Long clientId) {

        List<TicketResponse> ticketResponses = ticketResponseRepository.findByRequestIdAndClientId(requestId, clientId);
        log.info(ticketResponses.size() + " comentários encontrados para o ticket " + requestId + " do cliente " + clientId);

       return ticketResponses;
    }

    @Transactional
    public TicketResponse insert(TicketResponseDTO ticketResponse) {


        TicketResponse newComment = convertDTO(ticketResponse);
        newComment.setId(null);
        newComment.setResponseTime(new Date());

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        PortalUserDetails details = (PortalUserDetails) authentication.getPrincipal();

        Optional<Client> client = clientRepository.findByUsername(details.getUsername());
        newComment.setClient(client.get());

        log.info("Novo comenetário do usuário " + client.get().getUsername() + " foi adicionado.");

        return ticketResponseRepository.save(newComment);
    }


    public TicketResponse convertDTO(TicketResponseDTO dto) {

        TicketResponse ticketResponse = new TicketResponse();
        ticketResponse.setComments(dto.getComments());

        Optional<Request> request = requestRepository.findById(dto.getRequestId());
        ticketResponse.setRequest(request.get());

        return ticketResponse;
    }

    public Request convertDTO(NewRequestDTO requestDTO) {

        Request request = new Request();
        request.setSubject(requestDTO.getSubject());
        request.setDescription(requestDTO.getDescription());
        request.setSeverity(requestDTO.getSeverity());

        return request;
    }

}
