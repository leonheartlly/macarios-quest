package com.requests.portal.service.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Constraint( validatedBy = ContactInsertValidator.class )
@Target( { ElementType.TYPE } )
@Retention( RetentionPolicy.RUNTIME )
public @interface ContactInsert {

    String message() default "Validation Error";


    Class< ? >[] groups() default {};


    Class< ? extends Payload>[] payload() default {};

}
