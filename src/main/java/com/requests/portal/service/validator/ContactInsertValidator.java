package com.requests.portal.service.validator;


import com.requests.portal.dto.user.GenericClient;
import com.requests.portal.resources.exceptions.FieldMessage;
import org.springframework.util.ObjectUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;


public class ContactInsertValidator implements ConstraintValidator< ContactInsert, GenericClient> {

    @Override
    public void initialize( ContactInsert ann ) {

    }

    /**
     * Custom validations to Client.
     * @param dto new product contact.
     * @param context
     * @return is valid.
     */
    @Override
    public boolean isValid( GenericClient dto, ConstraintValidatorContext context ) {

        List<FieldMessage> invalidList = new ArrayList<>();

        if (!ObjectUtils.isEmpty(dto)  ) {

            if(dto.getAgency().getCode() <= 0 || dto.getAgency().getCode() > 4){
               invalidList.add(new FieldMessage("agency", "Orgão inserido é inválido."));
            }else{

            }
        } else {


        }

        for ( FieldMessage e : invalidList ) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate( e.getMessage() ).addPropertyNode( e.getFieldName() ).addConstraintViolation();
        }

        return invalidList.isEmpty();
    }
}
