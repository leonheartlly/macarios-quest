package com.requests.portal.service.Exceptions;


public class DataProtectionException extends RuntimeException {

    /**
     * Recebe mensagem de excessão.
     *
     * @param msg
     */
    public DataProtectionException(String msg ) {

        super( msg );

    }


    /**
     * Recebe a mensagem da excessão e uma causa ocorrida antes e lançada.
     *
     * @param msg
     *            mensagem da excessão.
     * @param cause
     *            Causa lançada recebida.
     */
    public DataProtectionException(String msg, Throwable cause ) {

        super( msg, cause );
    }
}
