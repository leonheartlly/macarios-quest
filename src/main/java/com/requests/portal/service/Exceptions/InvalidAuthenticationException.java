package com.requests.portal.service.Exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.naming.AuthenticationException;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidAuthenticationException extends AuthenticationException {

    /**
     * Recebe mensagem de excessão.
     *
     * @param msg
     */
    public InvalidAuthenticationException(String msg ) {

        super( msg );

    }

}
