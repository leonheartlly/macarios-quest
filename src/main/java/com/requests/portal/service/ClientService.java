package com.requests.portal.service;

import com.requests.portal.domain.Client;
import com.requests.portal.domain.Role;
import com.requests.portal.dto.user.NewClientDTO;
import com.requests.portal.dto.user.PortalUserDetails;
import com.requests.portal.dto.user.UserPasswordChange;
import com.requests.portal.mail.EmailDTO;
import com.requests.portal.mail.PortalMailSender;
import com.requests.portal.repository.ClientRepository;
import com.requests.portal.repository.ClientRepositoryUserDetails;
import com.requests.portal.repository.RoleRepository;
import com.requests.portal.service.Exceptions.DataIntegrityException;
import com.requests.portal.service.Exceptions.DataProtectionException;
import com.requests.portal.service.Exceptions.ObjectNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class ClientService implements UserDetailsService {


    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private PortalMailSender mail;

    private static final Long ROLE_USER = 3l;


    public List<Client> listAll() {

        List<Client> clients = clientRepository.findAll();

        return clients;
    }

    //sobrecarregar com objeto generico
    @Override
    public PortalUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<Client> client = clientRepository.findByUsername(username);

        return new ClientRepositoryUserDetails(client.get());
    }

    public Client findAuthenticatedUser(String username){

        Optional<Client> client = clientRepository.findByUsername(username);

        return client.orElseThrow(() -> new ObjectNotFoundException("O objeto solicitado não foi encontrado. Username: " + username + ", Tipo: " + Client.class.getSimpleName()));
    }

    public Client findById(Long id) {

        Optional<Client> client = clientRepository.findById(id);

        return client.orElseThrow(
                () -> new ObjectNotFoundException("O objeto solicitado não foi encontrado. Id: " + id + ", Tipo: " + Client.class.getSimpleName())
        );
    }

    @Transactional
    public Client insert(Client client) {

        try {
            client.setId(null);
            client.setEnabled(false);

           EmailDTO email =  mail.newUserEmailMessage(client.getUsername(), client.getEmail());
           mail.sendEmail(email);

            return clientRepository.save(client);
        }catch(DataIntegrityViolationException sqlex){
            log.error("Tentativa de inserção do usuário: " + client.getUsername() + "\n " + sqlex.getMessage());
            throw new DataIntegrityException("O usuário solicitado não pode ser inserido.");
        }
    }

    public void delete(Long id) {

        try {
            findById(id);

            clientRepository.deleteById(id);
        } catch (DataIntegrityViolationException dive) {
            log.error(dive);
            throw new DataIntegrityException("Não foi possível efetuar a remoção.");
        }
    }


    @Transactional
    public Client update(Client client) {

        Client old = findById(client.getId());

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails details = (UserDetails) authentication.getPrincipal();

        if (validateAuthenticatedUpdate(details, old) && validateRoleUpdate(details, client)) {
            mapper.map(client, old);

            if (client.getPwd() != null) {
                old.setPwd(passwordEncoder.encode(client.getPwd()));
            }

            log.info("Usuário " + old.getUsername() + " alterado com sucesso.");
            return clientRepository.save(old);
        }

        return client;
    }

    public Client passwordChange(UserPasswordChange userPasswordChange) {

        Optional<Client> old = clientRepository.findByUsernameAndEmail(userPasswordChange.getUsername(), userPasswordChange.getEmail());

        if (old.isPresent()) {
            old.get().setPwd(passwordEncoder.encode(userPasswordChange.getPassword()));
            clientRepository.save(old.get());

            log.info("A senha do usuário " + old.get().getUsername() + " foi alterada com sucesso.");
            return old.get();
        }

        log.info("A senha do usuário " + userPasswordChange.getUsername() + " não pode ser alterada. Usuário não encontrado.");
        throw new ObjectNotFoundException("Objeto solicitado não foi encontrado. " + Client.class.getSimpleName());
    }

    //TODO implementar bloqueio de usuário através de tentativas inválidas
    @Transactional
    public void enableDisableClient(boolean enable, Client client) {

        Client enabledUser = findById(client.getId());
        EmailDTO email = mail.userEnabled(enabledUser.getUsername(), enabledUser.getEmail());
        mail.sendEmail(email);

        clientRepository.setEnableDisableClientById(enable, client.getId());
    }

    public Client convertDTO(NewClientDTO clientDTO) {

        Client client = new Client();
        client.setName(clientDTO.getName());
        client.setPwd(passwordEncoder.encode(clientDTO.getPassword()));
        client.setEmail(clientDTO.getEmail());
        client.setAgency(clientDTO.getAgency());
        client.setFone(clientDTO.getFone());
        client.setUsername(clientDTO.getUsername());

        try{

            List<Role> roles = roleRepository.findAllById( clientDTO.getRoles() );

            client.setRoles(roles);
        }catch (Exception ex){

            log.error(ex);
        }

        return client;
    }

    public List<Role> findRoles(List<Long> rolesId){


        List<Role> roles = roleRepository.findAllById( rolesId );

        return roles;
    }

    public boolean validateAuthenticatedUpdate(UserDetails details, Client client) {

        //ROLE_USER só pode alterar o próprio perfil.
        boolean isValid = client.getRoles().stream().anyMatch(
                role -> role.getId() != ROLE_USER || client.getUsername().equalsIgnoreCase(details.getUsername())
        );

        if (isValid ) {
            return true;
        }

        //TODO adicionar validação por fone, enviando sms para confirmar

        log.warn("Tentativa de alteração de usuário inválida. Usuário  " + client.getUsername());
        throw new DataProtectionException("Este usuário não tem permissão para efetuar a ação desejada.");
    }

    /**
     * Valida se o usuário tem permissão suficiente para alterar as permissões do usuário.
     * @param client Roles solicitadas.
     * @return permissao suficiente?
     */
    private boolean validateRoleUpdate(UserDetails details, Client client){

        Client authenticated = this.findAuthenticatedUser(details.getUsername());

//        List<Role> roles = roleRepository.findAllById( client.getRoles() );

//        Set<Role> authenticatedRoles = new HashSet<>(authenticated.getRoles());

        boolean containsSuperRole = client.getRoles().stream().anyMatch(role -> role.getId() != ROLE_USER);
        boolean isAdmin = authenticated.getRoles().stream().anyMatch( role -> role.getId() != ROLE_USER);

        if(!containsSuperRole || isAdmin){

            return true;
        }

        return false;
    }

}
