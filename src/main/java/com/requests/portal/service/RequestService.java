package com.requests.portal.service;

import com.requests.portal.domain.Client;
import com.requests.portal.domain.Request;
import com.requests.portal.domain.enuns.Status;
import com.requests.portal.dto.NewRequestDTO;
import com.requests.portal.dto.RequestDTO;
import com.requests.portal.dto.user.PortalUserDetails;
import com.requests.portal.repository.ClientRepository;
import com.requests.portal.repository.RequestRepository;
import com.requests.portal.service.Exceptions.DataIntegrityException;
import com.requests.portal.service.Exceptions.ObjectNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class RequestService {


    @Autowired
    private RequestRepository requestRepository;


    @Autowired
    private ClientRepository clientRepository;


    public List<Request> listAll() {

        List<Request> clients = requestRepository.findAll();

        return clients;
    }

    public Request findById(Long id) {

        Optional<Request> client = requestRepository.findById(id);

        return client.orElseThrow(
                () -> new ObjectNotFoundException("O objeto solicitado não foi encontrado. Id: " + id + ", Tipo: " + Request.class.getSimpleName())
        );
    }

    @Transactional
    public Request insert(Request request) {

        request.setId(null);
        request.setRequestTime(new Date());
        request.setStatus(Status.OPEN.getCode());
        //Obter a agencia do usuario

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        PortalUserDetails details = (PortalUserDetails) authentication.getPrincipal();

        Optional<Client> client = clientRepository.findByUsername(details.getUsername());
        request.setClient(client.get());
        request.setAgency(client.get().getAgency().getCode());



        return requestRepository.save(request);
    }

    public void delete(Long id) {

        try {
            findById(id);

            requestRepository.deleteById(id);
        }catch (DataIntegrityViolationException dive) {
            log.error(dive);
            throw new DataIntegrityException("Não foi possível efetuar a remoção.");
        }
    }


    public Request update(Request client) {

        Request old = findById(client.getId());

        updateData(old, client);
        return requestRepository.save(old);
    }

    private void updateData( Request oldRequest, Request request ) {

        oldRequest.setDescription( request.getDescription() );
        oldRequest.setSubject(request.getSubject());
        oldRequest.setAgency(request.getAgency());
    }

    public Request convertDTO(RequestDTO requestDTO) {

        Request client = new Request();
        requestDTO.setDescription(requestDTO.getDescription());

        return client;
    }

    public Request convertDTO(NewRequestDTO requestDTO) {

        Request request = new Request();
        request.setSubject(requestDTO.getSubject());
        request.setDescription(requestDTO.getDescription());
        request.setSeverity(requestDTO.getSeverity());

        return request;
    }

}
