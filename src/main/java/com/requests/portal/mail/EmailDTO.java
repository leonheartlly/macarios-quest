package com.requests.portal.mail;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EmailDTO {

    public String subjeect;

    public String body;

    public String mailTo;

}
