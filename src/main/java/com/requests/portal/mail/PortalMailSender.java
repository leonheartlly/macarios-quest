package com.requests.portal.mail;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Slf4j
@Service
public class PortalMailSender{

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("spring.mail.username")
    private String mailFrom;

    /**
     * Envia email para um destinatario solicitado. O emial de envio é fixo.
     * @param dto email
     */
    public void sendEmail(EmailDTO dto) {

        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper msg = new MimeMessageHelper (message);
        try {
            msg.setTo(dto.getMailTo());
            msg.setFrom(mailFrom);
            msg.setSubject(dto.getSubjeect());
            msg.setText(dto.getBody());
        }catch (MessagingException e){

            log.error("Não foi possível enviar email para " + dto.getMailTo() + ". Error: " + e.getMessage());
        }

        log.info("Email enviado para "+ dto.getMailTo() + "\n Assunto: " + dto.getSubjeect());

        javaMailSender.send(message);

    }

    /**
     * Formaata o email de boas vindas a novos usuários.
     * @param usuario novo usuário cadastrado.
     * @param mailTo email do novo usuário.
     * @return Email formatado.
     */
    public EmailDTO newUserEmailMessage(String usuario, String mailTo) {

        return EmailDTO.builder()
                .body(String.format("PORTAL MACARIO QUEST\n O usuário <b>%s</b> foi cadastrado com sucesso!\n Espere!\n\n Assim que o administrador do sistema validar seu cadastro, você receberá um novo email e poderá começar a usar o sistema.", usuario))
                .subjeect("Novo usuário cadastrado no Portal Macário Quest")
                .mailTo(mailTo)
                .build();
    }

    /**
     *
     * @param usuario
     * @param mailTo
     * @return
     */
    public EmailDTO userEnabled(String usuario, String mailTo){
        return EmailDTO.builder()
                .body(String.format("PORTAL MACARIO QUEST\n O usuário <b>%s</b> O usuário foi validado e agora esta liberado para usar o sistema!\n Acesse o link ", usuario))
                .subjeect("Usuário Liberado")
                .mailTo(mailTo)
                .build();
    }

}
