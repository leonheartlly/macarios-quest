package com.requests.portal.repository;

import com.requests.portal.domain.TicketResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketResponseRepository extends JpaRepository<TicketResponse, Long > {

    List<TicketResponse> findByRequestId(Long requestId);

    List<TicketResponse> findByRequestIdAndClientId(Long requestId, Long clientId);
}
