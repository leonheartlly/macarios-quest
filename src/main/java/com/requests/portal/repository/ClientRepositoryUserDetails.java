package com.requests.portal.repository;

import com.requests.portal.domain.Client;
import com.requests.portal.dto.user.PortalUserDetails;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class ClientRepositoryUserDetails extends Client implements PortalUserDetails {

    public ClientRepositoryUserDetails(Client client) {
        super(client);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles();
    }

    @Override
    public String getPassword() {
        return super.getPwd();
    }

    @Override
    public Long getId(){
        return super.getId();
    }

    @Override
    public boolean getEnabled() {
        return super.isEnabled();
    }

    @Override
    public Client getClient() {
        Client cli = new Client();
        cli.setId(this.getId());
        cli.setUsername(this.getUsername());

        //TODO criar builder aqui
        return  cli;
    }


    @Override
    public String getUsername() {
        return super.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return super.isEnabled();
    }
}
