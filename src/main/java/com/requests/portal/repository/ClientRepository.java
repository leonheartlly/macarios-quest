package com.requests.portal.repository;

import com.requests.portal.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long > {

    Optional<Client> findByUsername(String username);

    Optional<Client> findByUsernameAndEmail(String username, String email);

    @Modifying
    @Query("update Client cli set cli.enabled = ?1 where cli.id = ?2")
    void setEnableDisableClientById(boolean enable, Long id);
}
