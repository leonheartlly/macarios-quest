package com.requests.portal.utils;

import com.requests.portal.repository.ClientRepository;
import com.requests.portal.repository.ClientRepositoryUserDetails;
import com.requests.portal.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import java.util.Arrays;
import java.util.Base64;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableResourceServer
public class OAuth2ServerConfiguration extends ResourceServerConfigurerAdapter {


    private static final String RESOURCE_ID = "portalservice";



    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {

        resources.resourceId(RESOURCE_ID);
    }


    @Override
    public void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.logout()
                .invalidateHttpSession(true)
                .clearAuthentication(true)

                .and().authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers(HttpMethod.POST, "/usuarios/novo-usuario").permitAll()
                .anyRequest().fullyAuthenticated();
    }


    @Configuration
    @EnableAuthorizationServer
    protected static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

        private TokenStore tokenStore = new InMemoryTokenStore(); //poderia armazenar esse token no banco

        @Autowired
        @Qualifier("authenticationManagerBean")
        private AuthenticationManager authenticationManager;


        @Value("${secret-key:889977}")
        private static String secret;//rever

        @Autowired
        private ClientService clientService;

        @Autowired
        @Qualifier("dataSource")
        private DataSource dataSource;

        private ClientRepositoryUserDetails clientRepositoryUserDetails;

        @Autowired
        private PasswordEncoder passwordEncoder;

//        @Bean
//        public TokenStore tokenStore() {
//            return new JwtTokenStore(this.tokenStore);
//        }

//        @Bean
//        public JwtAccessTokenConverter accessTokenConverter() {
//            JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
//            converter.setSigningKey("123");
//            return converter;
//        }

        @PostConstruct
        public void init(){
            secret = Base64.getEncoder().encodeToString(secret.getBytes());
        }

        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpointsConfigurer) {
            endpointsConfigurer.tokenStore(this.tokenStore)
                    .authenticationManager(this.authenticationManager)
                    .userDetailsService(clientService);
        }

        //como armazena o token
        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            clients.inMemory()
                    .withClient("client")
                    .authorizedGrantTypes("password", "authorization_code", "refresh_token").scopes("all")
            .refreshTokenValiditySeconds(300000)
            .resourceIds(RESOURCE_ID)
            .secret(passwordEncoder.encode("889977"))//configurar uma senha difícil
            .accessTokenValiditySeconds(50000) ;

//            JdbcClientDetailsService jdbc = new JdbcClientDetailsService(dataSource);
//            clients.withClientDetails(jdbc)
//                    .withClient("client")
//                    .authorizedGrantTypes("password", "authorization_code", "refresh_token").scopes("all")
//                    .refreshTokenValiditySeconds(300000)
//                    .resourceIds(RESOURCE_ID)
//                    .secret(passwordEncoder.encode("889977"))//configurar uma senha difícil
//                    .accessTokenValiditySeconds(50000);
        }

        @Bean
        @Primary
        public DefaultTokenServices tokenServices() {

            DefaultTokenServices tokenServices = new DefaultTokenServices();
            tokenServices.setSupportRefreshToken(true);
            tokenServices.setTokenStore(this.tokenStore);

            return tokenServices;
        }

    }
}
