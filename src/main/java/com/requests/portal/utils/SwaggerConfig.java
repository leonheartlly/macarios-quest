package com.requests.portal.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.Collections;


@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {

        return new Docket( DocumentationType.SWAGGER_2 )
                .select()
                .apis( RequestHandlerSelectors.basePackage( "com.requests.portal" ) )
                .paths( PathSelectors.any() )
                .build()
                .apiInfo( apiInfo() );
    }


    private ApiInfo apiInfo() {

        return new ApiInfo( "Macario Quest", "", "Versão 1.0", "", new Contact( "Cleanto Macário", "", "lucasf.silva@outlook.com" ), "", "",
            Collections.emptyList() );

    }


    @Bean
    public CorsFilter corsFilter() {

        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials( true );
        config.setAllowedOrigins( Collections.singletonList( "*" ) );//TODO configurar o cors para o projeto final
        config.setAllowedHeaders( Arrays.asList( "Origin", "Content-Type", "Accept" ) );
        config.setAllowedMethods( Arrays.asList( "GET", "POST", "PUT", "OPTIONS", "DELETE", "PATCH" ) );
        source.registerCorsConfiguration( "/**", config );
        return new CorsFilter( source );
    }
}
