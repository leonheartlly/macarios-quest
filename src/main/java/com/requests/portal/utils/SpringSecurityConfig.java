package com.requests.portal.utils;

import com.requests.portal.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private ClientService clientService;




    // Create 2 users for demo
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(clientService);
       /* auth.inMemoryAuthentication()
                .withUser("username").password(passwordEncoder().encode("password")).roles("ADMIN")
                .and()
                .withUser("admin").password("{noop}password").roles("USER", "ADMIN");*/

    }

    // Secure the endpoins with HTTP Basic authentication
   /* @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.requestMatchers()
                .antMatchers("/**")
            .and().authorizeRequests()
        .antMatchers("/oauth/authorize", "/swagger**").permitAll();

//        http
//                //HTTP Basic authentication
//                .httpBasic()
//                .and()
//                .authorizeRequests()
//                .antMatchers(HttpMethod.GET, "/desafio/**").hasRole("USER")
//                .antMatchers(HttpMethod.POST, "/desafio").hasRole("ADMIN")
//                .antMatchers(HttpMethod.PUT, "/desafio/**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.PATCH, "/desafio/**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.DELETE, "/desafio/**").hasRole("ADMIN")
//                .and()
//                .csrf().disable()
//                .formLogin().disable();
    }*/



    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {

        return super.authenticationManagerBean();
    }

    @Override
    public void configure(WebSecurity webSecurity){
        webSecurity.ignoring().antMatchers(HttpMethod.OPTIONS, "/**").antMatchers("/v2/api-docs","/configuration/ui","/swagger-resources/**","/swagger-ui.html","/webjars/**");
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

}
